# terragrunt.hcl example
remote_state {
  backend = "s3"
  config = {
    bucket         = "terraform-live-minikube-${get_aws_account_id()}"
    dynamodb_table = "terraform-live-minikube-${get_aws_account_id()}"
    key            = "${path_relative_to_include()}/terraform.tfstates"
    encrypt        = true
    region         = "eu-central-1" 
  }   
}