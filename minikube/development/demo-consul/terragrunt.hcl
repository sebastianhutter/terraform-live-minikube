
terraform {
    source = "git::git@gitlab.com:sebastianhutter/terraform-modules.git//kubernetes/demo-consul-101"
}

dependency "namespace" {
  config_path = "../namespace"
}

inputs = {
  consul_version = "v0.10.0"
  namespace = dependency.namespace.outputs.name
}

include {
  path = find_in_parent_folders()
}
