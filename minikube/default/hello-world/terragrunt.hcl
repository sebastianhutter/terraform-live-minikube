
terraform {
    source = "git::git@gitlab.com:sebastianhutter/terraform-modules.git//kubernetes/hello-world"
}

include {
  path = find_in_parent_folders()
}
