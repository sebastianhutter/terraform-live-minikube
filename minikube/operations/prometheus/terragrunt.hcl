
terraform {
    source = "git::git@gitlab.com:sebastianhutter/terraform-modules.git//kubernetes/prometheus"
}

dependency "namespace" {
  config_path = "../namespace"
}

inputs = {
  namespace = dependency.namespace.outputs.name
}

include {
  path = find_in_parent_folders()
}

