
terraform {
    source = "git::git@gitlab.com:sebastianhutter/terraform-modules.git//kubernetes/namespace"
}

inputs = {
  name = "operations"
}

include {
  path = find_in_parent_folders()
}
