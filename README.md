# terraform-live-minikube

Example terraform platform for local development based on minikube.

## Setup

### Install required softwarew

First install terraform, terragrunt and minikube

```bash
brew cask install minikube
brew install terraform terragrunt kubernetes-helm
```


### Prepare minikube
Now, create a new local kubernetes cluster.
*Attention*: We need to use an older kubernetes version (< 1.16) until this bug is fixed: https://github.com/kubernetes/minikube/issues/5429

```bash
minikube start --kubernetes-version=v1.15.4
```

In a new session setup the minikube tunnel or else all kubernetes services
wont be able to receive a dynamic ip which will cause terraform modules with `kubernetes_services` to loop endlessly.

```bash
minikube tunnel
```

### Setup aws credentials

You also need an AWS account to use the s3 storage backend.

Copy the file `.env.example` to `.env` and insert your AWS credentials. 


## Setup helm v2
*helm needs tiller installed - with v3 the requirement for the tiller service dissappears - lets hope we are getting there soon*

Initalize helm
```bash
helm init
```

## Run terraform / terragrunt

With minikube running and your AWS account ready you can run the terraform setup by running the command:

```bash
# setup operations namespace with consul
cd  minikube/operations
terragrunt apply-all

# setup consul connect demo
cd ../development
terragrunt apply-all
```

Afterwards start the minikube dashboard and check the deployed services
```bash
minikube dashboard
```

To print the service endpoints use 
```
minikube service list
```